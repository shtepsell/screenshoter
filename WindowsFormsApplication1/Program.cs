﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace WindowsFormsApplication1
{

    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            Form1 f = new Form1();
//            RegisterHotKey(new IntPtr(0), f.GetHashCode(), (int)Keys.Control, (int)Keys.F8);
            //WmHotkeyMessageFilter messageFilter = new WmHotkeyMessageFilter(f);
            Application.Run();
//            UnregisterHotKey(new IntPtr(0), f.GetHashCode());
            
        }
    }
}


