﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        const int HOTKEY_ID = 31197;
        private string lastKey;
        private string lastKeyMod;
        private string lastUrl;
        private static string siteUrl = "http://img.ewg.su";
        private static string uploadUrl = siteUrl + "/upload.php";
        private static string progName = "ScreenShoter.exe";
        private string tbCtrlText;
        private string tbCtrlAWText;

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);
        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);
        /*
        [DllImport("user32.dll")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        */
        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);
        [StructLayout(LayoutKind.Sequential)]
        private struct Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        public enum KeyModifiers        //enum to call 3rd parameter of RegisterHotKey easily
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            Windows = 8
        }

        public bool setHotKey(KeyModifiers Kmds, Keys key)
        {
            return RegisterHotKey(this.Handle, HOTKEY_ID, (uint)Kmds, (uint)key);
        }
        public bool unSetHotKey()
        {
            return UnregisterHotKey(this.Handle, HOTKEY_ID);
        }
        const int WM_HOTKEY = 0x0312;

        protected override void WndProc(ref Message message)
        {
            switch (message.Msg)
            {
                case WM_HOTKEY:
                    //Keys key = (Keys)(((int)message.LParam >> 16) & 0xFFFF);
                    //KeyModifiers modifier = (KeyModifiers)((int)message.LParam & 0xFFFF);
                    //put your on hotkey code here
                    //MessageBox.Show("HotKey Pressed :" + modifier.ToString() + " " + key.ToString());
                    //end hotkey code
                    /*
                    MessageBox.Show(((KeyModifiers)((int)message.LParam & 0xFFFF)).ToString());
                    MessageBox.Show(((Keys)(((int)message.LParam >> 16) & 0xFFFF)).ToString());
                    */
                    lastKeyMod = ((KeyModifiers)((int)message.LParam & 0xFFFF)).ToString();
                    lastKey = ((Keys)(((int)message.LParam >> 16) & 0xFFFF)).ToString();
                    someact();

                    break;
            }
            base.WndProc(ref message);
        }

        /// <summary>
        /// //////////////////////////////
        /// </summary>
        
        //////////////////////
        private static WINDOWPLACEMENT GetPlacement(IntPtr hwnd)
        {
            WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
            placement.length = Marshal.SizeOf(placement);
            GetWindowPlacement(hwnd, ref placement);
            return placement;
        }

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetWindowPlacement(
            IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        internal struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public ShowWindowCommands showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        internal enum ShowWindowCommands : int
        {
            Hide = 0,
            Normal = 1,
            Minimized = 2,
            Maximized = 3,
        }
        /// <summary>
        /// //////////////////
        /// </summary>


        public Form1()
        {
            InitializeComponent();

            updater();

            //MessageBox.Show("text", "title", MessageBoxButtons.OK,MessageBoxIcon.Asterisk,MessageBoxDefaultButton.Button1);
            //this.WindowState = FormWindowState.Minimized;
            //this.ShowInTaskbar = false;

            //put this code in the onload method of your form
            //setHotKey(KeyModifiers.Alt | KeyModifiers.Shift, Keys.S);
            setHotKey(KeyModifiers.Control, Keys.Up);
            setHotKey(KeyModifiers.Control, Keys.Down);
            // from txt to keys
            /*
            string s = "Up";
            Keys k = (Keys)Enum.Parse(typeof(Keys), s);
            MessageBox.Show(k.ToString(), "test k");
            */
            //from txt to key mod
            /*
            string sm = "Control";
            KeyModifiers km = (KeyModifiers)Enum.Parse(typeof(KeyModifiers),sm);
            MessageBox.Show(km.ToString(), "test km");
            */
        }

        private void updater()
        {
            //Encoding.ASCII.GetString(resp)
            //MessageBox.Show(Encoding.ASCII.GetString(resp) + "\r\n" + this.ProductVersion.ToString());
            string ver = getVer();
            if (ver != this.ProductVersion.ToString() && ver != null)
            {
                MessageBox.Show("need upd");
            }
            else
            {
                MessageBox.Show("no upd");
                //upd proccess
                
                Process p = new Process();
                p.StartInfo.UseShellExecute = false;
                File.AppendAllText("upd.bat", "taskkill /im " + progName + "\r\ncopy " + progName + " " + progName + ".bak1\r\ndel /f /q upd.bat\r\n" + progName);
                //WebClient.DownloadFile here
                p.StartInfo.FileName = "upd.bat";
                p.Start();
                
            }
        }

        private string getVer()
        {
            WebClient wc = new WebClient();
            byte[] resp;
            try
            {
                resp = wc.DownloadData(siteUrl + "/ver.php");
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            return Encoding.ASCII.GetString(resp);
        }

        private void someact()
        {
            ///////////
            var rect = new Rect();
            const int nChars = 256;
            uint pid;
            string someout;
            StringBuilder Buff = new StringBuilder(nChars);

            //get active proc handle
            IntPtr handle = GetForegroundWindow();

            //get state
            var st = GetPlacement(handle);

            //get active window proccess info
            GetWindowThreadProcessId(handle, out pid);
            Process p = Process.GetProcessById((int)pid);
            someout = "\r\npMWT:" + p.MainWindowTitle + ";\r\n";
            someout += "pname:" + p.ProcessName + ";\r\n";
            //someout += "pTest:"+p.ToString()+";\r\n";
            //someout += "pFname:" + p.MainModule.FileName + ";\r\n";
            someout += "pFver:" + p.MainModule.FileVersionInfo + ";\r\n";
            //someout += "pMname:" + p.MainModule.ModuleName + ";\r\n";
            //someout += "pMsite:" + p.MainModule.Site + ";\r\n";
            //someout += "pBadr:" + p.MainModule.BaseAddress + ";\r\n";

            //this.Text = p.MainWindowTitle+" state: "+st.showCmd.ToString();

            //get active window coords
            GetWindowRect(handle, ref rect);
            /*
            string rs = "\r\nt:" + rect.Top.ToString()
                + ";\r\nr:" + rect.Right.ToString()
                + ";\r\nb:" + rect.Bottom.ToString()
                + ";\r\nl:" + rect.Left.ToString();
            someout = rs + someout;
            */ 

            //get active window title
            /*
            if (GetWindowText(handle, Buff, nChars) > 0)
            {
                someout = Buff.ToString() + someout;
            }
            else someout = "none " + someout;
            */

            //MessageBox.Show(someout);
            //MessageBox.Show(p.MainWindowTitle);
            //MessageBox.Show("t - " + rect.Top.ToString());
            //& " r - " & rect.Right.ToString());
            //Bitmap scr = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            
            int rH, rW, scrH, scrW;
            if (lastKey == "Down" && p.MainWindowTitle != "")
            {
                rW = rect.Right - rect.Left - 16;
                if (st.showCmd.ToString() == "Maximized") rH = rect.Bottom - rect.Top - 16;
                else rH = rect.Bottom - rect.Top - 16 - 22;
                scrW = rect.Left + 8;
                if (st.showCmd.ToString() == "Maximized") scrH = rect.Top + 8;
                else scrH = rect.Top + 8 + 22;
            }
            else
            {
                rW = Screen.PrimaryScreen.Bounds.Width;
                rH = Screen.PrimaryScreen.Bounds.Height;
                scrW = 0;
                scrH = 0;
            }
            Bitmap scr = new Bitmap(rW, rH);
            Graphics grp = Graphics.FromImage(scr as Image);
            grp.CopyFromScreen(scrW, scrH, 0, 0, scr.Size);
            /*
            Bitmap scr = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics grp = Graphics.FromImage(scr as Image);
            grp.CopyFromScreen(0, 0, 0, 0, scr.Size);
            */
            /*
            pictureBox1.Image = scr;
            pictureBox1.Height = scr.Height / 2;
            pictureBox1.Width = scr.Width / 2;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            this.Width = (scr.Width / 2)+50;
            this.Height = (scr.Height / 2)+50;
            */
            //MessageBox.Show(rW + "x" + rH + "x" + rW + "x" + rH + "\r\n\r\n" + someout);
            ///////////
            /*
            Bitmap scr = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics grp = Graphics.FromImage(scr as Image);
            grp.CopyFromScreen(0, 0, 0, 0, scr.Size);
            */
            lastUrl = PostIMG(scr);
            if(lastUrl == null) 
            {
                notifyIcon1.BalloonTipTitle = "Ошибка при загрузке";
                notifyIcon1.BalloonTipText = "При попытке загрузки изображения возникла ошибка";
                notifyIcon1.ShowBalloonTip(1000);
            }
            else 
            {
                Clipboard.SetText(lastUrl, TextDataFormat.Text);
                notifyIcon1.BalloonTipTitle = "Снимок загружен";
                notifyIcon1.BalloonTipText = lastUrl;
                notifyIcon1.ShowBalloonTip(1000);
            }
        }

        private static string PostIMG(Bitmap img)
        {
            byte[] responseArray;
            WebClient myWebClient = new WebClient();
            NameValueCollection myNameValueCollection = new NameValueCollection();
            MemoryStream msimg = new MemoryStream();
            img.Save(msimg, ImageFormat.Jpeg);
            myNameValueCollection.Add("img", Convert.ToBase64String(msimg.ToArray()));
            try
            {
                responseArray = myWebClient.UploadValues( uploadUrl, "POST", myNameValueCollection);
            }
            catch (Exception e)
            {
                return null;
            }
            return Encoding.ASCII.GetString(responseArray);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            this.Hide();
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                //someact();
                notifyIcon1.BalloonTipText = "Щелкни дважды по значку\r\nили\r\nиспользуй сочетание клавиш\r\n<CTRL>+<стрелка вверх>\r\nдля снимка активного окна:\r\n<CTRL>+<стрелка вниз>";
                notifyIcon1.BalloonTipTitle = "Что бы сделать снимок";
                notifyIcon1.ShowBalloonTip(1000);
            }
        }

        // танцы вокруг скрытия/отображения окошка

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            this.Opacity = 100;
            this.Show();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            this.Hide();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            someact();
            /*
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;
                this.Visible = false;
                this.Hide();
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                this.Visible = true;
                this.Show();
                this.Activate();
            }
            */
        }


        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;
                this.Hide();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Form1_Closed(object sender, FormClosedEventArgs e)
        {
            //and set up a form closed event and call
            unSetHotKey();
        }

        private void снимокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            someact();
        }

        private void сайтToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(siteUrl);
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void tbCtrl_keyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show(Control.ModifierKeys.ToString() + " - " + e.KeyCode.ToString(), "some key down");
            if (e.KeyValue == 16 || e.KeyValue == 17 || e.KeyValue == 18)
            {
                tbCtrlText = e.Modifiers.ToString();
            }
            else
            {
                if (e.Modifiers.ToString() != "None" )
                {
                    tbCtrlText = e.Modifiers.ToString()+"+"+e.KeyCode.ToString();
                }
                else
                {
                    tbCtrlText = e.KeyCode.ToString();
                }
                
            }
            tbCtrl.Text = tbCtrlText;
            /*
            tbCtrlText = Control.ModifierKeys.ToString()
                + " -c " + e.KeyCode.ToString()
                + " -d " + e.KeyData.ToString()
                + " -v " + e.KeyValue.ToString()
                + " -m " + e.Modifiers.ToString();
            */
        }

        private void tbCtrl_textCh(object sender, EventArgs e)
        {
            tbCtrl.Text = tbCtrlText;
        }

        private void tbCtrlAW_keyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 16 || e.KeyValue == 17 || e.KeyValue == 18)
            {
                tbCtrlAWText = e.Modifiers.ToString();
            }
            else
            {
                if (e.Modifiers.ToString() != "None")
                {
                    tbCtrlAWText = e.Modifiers.ToString() + "+" + e.KeyCode.ToString();
                }
                else
                {
                    tbCtrlAWText = e.KeyCode.ToString();
                }

            }
            tbCtrlAW.Text = tbCtrlAWText;
        }

        private void tbCtrlAW_textCh(object sender, EventArgs e)
        {
            tbCtrlAW.Text = tbCtrlAWText;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("\r\nПрограмма предназначена для снятия снимка с экрана и активного приложения и моментальной загрузки на хостинг с помещением ссылки в буфер обмена.\r\nМожно было бы накрутить хоть до фотошопа, хоть до снятия снимка с выделеяемой части экрана, но таких хватает.", "О программе " + progName + " v." + this.ProductVersion, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        // ****** //
    }
}
